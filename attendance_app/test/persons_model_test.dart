import 'package:attendance_app/model/__mocks__/persons_mock.dart';
import 'package:attendance_app/model/api/api.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  final mockPersonMap = jsonPersons;
  final mockPersons = Persons.fromJson(mockPersonMap);


  test('should true when compare between mockPersons and persons from mockPersons.toJson()', () {

    final map = mockPersons.toJson();
    final personsReal = Persons.fromJson(map);

    expect(personsReal, mockPersons);
  });
}
