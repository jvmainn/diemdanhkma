import 'package:attendance_app/model/models.dart';
import 'package:equatable/equatable.dart';
import 'package:mqtt_client/mqtt_client.dart';

class AppSensors extends AppTopic with EquatableMixin{
  List<Param> _paramsSensor;

  AppSensors(String topicString, MqttQos qos, this._paramsSensor)
      : super(topicString: topicString, qos: qos);

  AppSensors.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    final data = json['params'];
    _paramsSensor = List();
    data.forEach((f) {
      _paramsSensor.add(Param.fromJson(f));
    });
  }

  List<Param> get paramsSensor => _paramsSensor;

  @override
  // TODO: implement props
  List<Object> get props => [paramsSensor];
}

class Param extends Equatable{
  double _temp, _hum, _smoke;
  int _person;
  String _time;
  MqttQos _qos;

  Param.fromJson(Map<String, dynamic> json) {
    _time = json['time'];
    switch(json['qos']){
      case 1:
        _qos = MqttQos.atLeastOnce;
        break;
      case 2:
        _qos = MqttQos.exactlyOnce;
        break;
      case 3:
        _qos = MqttQos.atMostOnce;
        break;
    }
    _temp = json['temp'];
    _hum = json['hum'];
    _smoke = json['smoke'];
    _person = json['persons'];
  }

  Param.fromTopic(String data, MqttQos qos) {
    final arr = data.split('#');
    _temp = double.parse(arr[0].split(':')[1]);
    _hum = double.parse(arr[1].split(':')[1]);
    _smoke = double.parse(arr[2].split(':')[1]);
    _person = int.parse(arr[3].split(':')[1]);
    _time = DateTime.now().millisecondsSinceEpoch.toString();
    _qos = qos;
  }

  int get person => _person;

  get smoke => _smoke;

  get hum => _hum;

  double get temp => _temp;

  String get time => _time;

  MqttQos get qos => _qos;

  @override
  // TODO: implement props
  List<Object> get props => [ _temp, _hum, _smoke, _person, _time, _qos];

  @override
  // TODO: implement stringify
  bool get stringify => true;
}
