import 'package:equatable/equatable.dart';
import 'api.dart';

class Classroom extends Equatable {
  String id, name, address;
  AppSensors appSensors;
  Remote remote;
  Devices devices;
  Persons persons;

  Classroom({this.id, this.name, this.address, this.persons, this.remote, this.devices, this.appSensors});

  @override
  // TODO: implement props
  List<Object> get props =>
      [id, name, address, appSensors, remote, devices, persons];

  @override
  // TODO: implement stringify
  bool get stringify => true;
}
