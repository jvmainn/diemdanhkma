final jsonPersons = {
  'topic_string': 'node_dt010132_id',
  'qos': 1,
  'persons': [
    {
      'id': 'CT020219',
      'full_name': 'Trương Việt Hoàng',
      'email': 'hoangvt979@gmail.com',
      'attendances': [
        {
          'time': '9/14/12/6/5/2020',
        }
      ]
    },
    {
      'id': 'AT140245',
      'full_name': 'Trần Đình Toàn',
      'email': 'toantran26106@gmail.com',
      'phone': '0981143700',
      'attendances': [
        {
          'time': '15/12/0/6/5/2020',
        }
      ]
    },
    {'id': 'DT010132', 'full_name': 'Trần Văn Thắng', 'attendances': []},
    {'id': 'GV01', 'full_name': 'Dương Phúc Phần', 'attendances': []},
    {'id': 'GV02', 'full_name': 'Nguyễn Hữu Sơn', 'attendances': []},
    {'id': 'GV03', 'full_name': 'Chu Thị Ngọc Quỳnh', 'attendances': []},
    {'id': 'GV04', 'full_name': 'Lại Hồng Nhung', 'attendances': []},
    {'id': 'GV05', 'full_name': 'Đặng Hùng Việt', 'attendances': []},
    {'id': 'GV06', 'full_name': 'Tô Thị Tuyết Nhung', 'attendances': []},
    {'id': 'GV07', 'full_name': 'Trần Ngọc Quý', 'attendances': []},
    {'id': 'GV08', 'full_name': 'Phùng Văn Quyền', 'attendances': []},
    {'id': 'GV09', 'full_name': 'Phạm Thị Thúy An', 'attendances': []},
    {'id': 'GV10', 'full_name': 'Vũ Thị Đào', 'attendances': []},
    {'id': 'GV11', 'full_name': 'Nguyễn Đức Công', 'attendances': []},
    {'id': 'GV12', 'full_name': 'Phạm Văn Hưởng', 'attendances': []},
    {'id': 'GV13', 'full_name': 'Nguyễn Chung Tiến', 'attendances': []},
    {'id': 'GV14', 'full_name': 'Đinh Tiến Thành', 'attendances': []},
    {'id': 'GV15', 'full_name': 'Phan Thị Thanh Huyền', 'attendances': []},
  ],
};
