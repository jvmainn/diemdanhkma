import 'package:attendance_app/bloc/app_bloc/bloc.dart';
import 'package:attendance_app/bloc/update_data_bloc/bloc.dart';
import 'package:attendance_app/provider/singletons/get_it.dart';
import 'package:attendance_app/views/home/classrooms_bloc/bloc.dart';
import 'package:attendance_app/views/home/home.dart';
import 'package:attendance_app/views/login/login.dart';
import 'package:attendance_app/views/router/router.dart';
import 'package:attendance_app/views/home/classrooms_bloc/classrooms_bloc.dart';
import 'package:attendance_app/views/splash/splash_screen.dart';
import 'package:attendance_app/views/widgets/dialogs/loading_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'configs/values/values.dart';

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<ClassroomsBloc>(
              create: (context) => locator<ClassroomsBloc>()),
          BlocProvider<UpdateDataBloc>(
              create: (context) => locator<UpdateDataBloc>()),
        ],
        child: GestureDetector(
          onTap: () {
            FocusScopeNode currentFocus = FocusScope.of(context);

            if (!currentFocus.hasPrimaryFocus &&
                currentFocus.focusedChild != null) {
              FocusManager.instance.primaryFocus.unfocus();
            }
          },
          child: MaterialApp(
            title: 'Điểm danh KMA',
            initialRoute: '/',
            onGenerateRoute: router(),
            debugShowCheckedModeBanner: false,
            theme: AppTheme.themeDefault,
            home: BlocConsumer<AppBloc, AppState>(
              listener: (context, state) {
                if (state is AppLoading) {
                  LoadingDialog.show(context, '');
                }else if(state is HideAppLoading){
                  LoadingDialog.hide(context);
                }
              },
              builder: (context, state) {
                ScreenUtil.init(context, width: 375, height: 812);
                if (state is AppAuthenticated) {
                  return BlocProvider.value(
                    value: BlocProvider.of<ClassroomsBloc>(context)
                      ..add(FetchClassrooms()),
                    child: InitHome(),
                  );
                }
                if (state is AppUnauthenticated) {
                  return LoginPage();
                }
                return SplashScreen();
              },
            ),
          ),
        ));
  }
}
