import 'dart:async';
import 'package:attendance_app/bloc/app_bloc/app_event.dart';
import 'package:attendance_app/bloc/app_bloc/bloc.dart';
import 'package:attendance_app/provider/local/authen_local_provider.dart';
import 'package:attendance_app/provider/local/persons_local_provider.dart';
import 'package:bloc/bloc.dart';

class AppBloc extends Bloc<AppEvent, AppState> {
  final AuthenLocalProvider authenLocalProvider;
  final PersonLocalProvider personLocalProvider;

  AppBloc(this.authenLocalProvider, this.personLocalProvider);

  @override
  AppState get initialState => AppUninitialized();

  @override
  Stream<AppState> mapEventToState(
    AppEvent event,
  ) async* {
    if (event is AppStarted) {
      final token = await authenLocalProvider.getTokenFromLocal();

      if (token != null) {
        yield AppAuthenticated();
      } else {
        yield AppUnauthenticated();
      }
    } else if (event is LoggedIn) {
      final result =
          await authenLocalProvider.saveTokenToLocal('this_is_token');
      yield AppAuthenticated();
    } else if (event is LogOuted) {
      yield AppLoading();
      final result =
      await authenLocalProvider.removeTokenFromLocal();
      yield HideAppLoading();
      yield AppUnauthenticated();
    }
  }
}
