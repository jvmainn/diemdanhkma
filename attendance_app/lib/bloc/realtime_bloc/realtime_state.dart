import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class RealTimeState extends Equatable{
  const RealTimeState();

  @override
  // TODO: implement props
  List<Object> get props => [];
}

class LoadingRealTime extends RealTimeState{}

class LoadedRealTime extends RealTimeState{

}

class FailedRealTime extends RealTimeState{
  final String error;

  FailedRealTime({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'Realtime Failure { error: $error }';
}

