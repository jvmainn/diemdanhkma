import 'package:attendance_app/bloc/update_data_bloc/bloc.dart';
import 'package:attendance_app/configs/values/values.dart';
import 'package:attendance_app/model/api/api.dart';
import 'package:attendance_app/views/classroom/tabs/attendance/attendance_tab.dart';
import 'package:attendance_app/views/widgets/appbar/appbar_primary.dart';
import 'package:attendance_app/views/widgets/widgets/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'tabs/control/control_device_tab.dart';

class ClassroomPage extends StatelessWidget {
  ClassroomPage({this.index});

  final int index;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UpdateDataBloc, UpdateDataState>(
      builder: (context, state) {
        if (state is LoadedData) {
          return _buildContent(context, state.classrooms[index]);
        }
        return AppLoadingWidget(
          mess: 'Xin chờ...',
        );
      },
    );
  }

  _buildContent(BuildContext context, Classroom classroom) {
    print('>>> _buildContent loaded class ${classroom.persons.toJson()}');

    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          title: Text(
            classroom.name.toString(),
            style: Theme.of(context)
                .primaryTextTheme
                .title
                .copyWith(fontSize: ScreenUtil().setSp(fzTitle)),
          ),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context),
          ),
        ),
        body: TabBarView(children: [
          ControlDeviceTab(
              appSensors: classroom.appSensors,
              devices: classroom.devices,
              remote: classroom.remote),
          AttendanceTab(persons: classroom.persons),
        ]),
        bottomNavigationBar: Container(
          height: ScreenUtil().setHeight(heightBottomTab),
          color: primary,
          child: TabBar(
            labelColor: bgWhite,
            indicatorColor: primary,
            unselectedLabelColor: borderWhite,
            tabs: <Widget>[
              Tab(
                child: Column(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.home),
                    Text('Điều khiển')
                  ],
                ),
              ),
              Tab(
                child: Column(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.userCheck),
                    Text('Điểm danh')
                  ],
                ),
              )
            ],
          ),
        ),
        // bottomNavigationBar:
      ),
    );
  }
}
