import 'package:attendance_app/configs/values/values.dart';
import 'package:attendance_app/views/widgets/appbar/appbar_primary.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';

import 'list_classroom.dart';
import 'list_practical_laboratory.dart';

class ClassroomsFragment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: colorIconWhite),
        title: Text('Hệ thống giám sát'),
        leading: IconButton(
          onPressed: () => Scaffold.of(context).openDrawer(),
          icon: Icon(Icons.sort),
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildHeader(context),
          Divider(
            color: primaryLight,
          ),
          ListPracticalLaboratory(),
          Divider(
            color: primary,
          ),
          Classrooms(),
        ],
      ),
    );
  }

  Widget _buildHeader(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          CircleAvatar(
            radius: 60,
            backgroundImage: AssetImage('assets/images/avatar.png'),
          ),
          Text(
            'Học viện kỹ thuật mật mã',
            style: Theme.of(context).primaryTextTheme.title.copyWith(
                fontSize: ScreenUtil().setSp(fzSubHead), color: primaryDark),
          ),
          Column(
            children: [
              Text(
                'Khoa DTVT',
                style: Theme.of(context).primaryTextTheme.title.copyWith(
                    fontSize: ScreenUtil().setSp(fzSubTitle), color: primaryDark),
              ),
              Text(
                '2019-2020',
                style: Theme.of(context).primaryTextTheme.title.copyWith(
                    fontSize: ScreenUtil().setSp(fzSubTitle), color: primaryDark),
              )
            ],
          ),
        ],
      ),
    );
  }
}
