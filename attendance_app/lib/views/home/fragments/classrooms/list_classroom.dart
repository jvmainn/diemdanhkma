import 'file:///F:/WorkSpace/OutSource/diem%20danh/attendance_app/lib/views/widgets/animations/delay_animation.dart';
import 'package:attendance_app/bloc/update_data_bloc/bloc.dart';
import 'package:attendance_app/configs/values/colors.dart';
import 'package:attendance_app/configs/values/font_size.dart';
import 'package:attendance_app/model/api/classroom.dart';
import 'package:attendance_app/views/widgets/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'item_classroom.dart';

class Classrooms extends StatelessWidget {
  final mockClassrooms = [Classroom(name: 'Phòng 1', address: 'TA1', id: 'p1')];

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            'DANH SÁCH PHÒNG HỌC',
            style: Theme.of(context).primaryTextTheme.title.copyWith(
                fontSize: ScreenUtil().setSp(fzSubTitle), color: primary),
          ),
        ),
        DelayedAnimation(
            offset: const Offset(0.35, 0),
            delay: 200,
            child: SizedBox(
              height: ScreenUtil().setHeight(150),
              child: ListView.builder(
                shrinkWrap: true,
                physics: const BouncingScrollPhysics(),
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) => ItemClassroom(
                    index: index, classroom: mockClassrooms[index]),
                itemCount: mockClassrooms.length,
              ),
            )),
      ],
    );
  }
}
