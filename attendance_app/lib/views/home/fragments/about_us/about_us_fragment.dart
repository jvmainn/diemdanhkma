import 'package:attendance_app/configs/values/colors.dart';
import 'package:attendance_app/configs/values/size_widget.dart';
import 'package:attendance_app/configs/values/values.dart';
import 'package:attendance_app/views/widgets/appbar/appbar_primary.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';

const String _aboutUs = '''
Ứng dụng giám sát phòng học
Phiên bản: 1.0.0
Bản quyền: Khoa DTVT 2020
''';

class AboutUsFragment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Thông tin ứng dụng',
          style: Theme.of(context)
              .primaryTextTheme
              .title
              .copyWith(fontSize: ScreenUtil().setSp(fzTitle)),
        ),
        leading: IconButton(
            icon: Icon(Icons.sort, color: Colors.white,),
            onPressed: () => Scaffold.of(context).openDrawer()),
      ),
      body: Padding(
        padding: EdgeInsets.all(ScreenUtil().setWidth(16)),
        child: Column(
          children: <Widget>[
            Center(
              child: Text(
                _aboutUs,
                style: Theme.of(context).primaryTextTheme.body1.copyWith(
                    fontSize: ScreenUtil().setSp(fzBody1),
                    color: colorTextBlack),
              ),
            )
          ],
        ),
      ),
    );
  }
}
