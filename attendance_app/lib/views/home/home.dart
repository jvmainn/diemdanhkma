import 'package:attendance_app/bloc/mqtt_bloc/bloc.dart';
import 'package:attendance_app/bloc/update_data_bloc/bloc.dart';
import 'package:attendance_app/views/home/classrooms_bloc/bloc.dart';
import 'package:attendance_app/views/home/drawer_bloc/bloc.dart';
import 'package:attendance_app/views/home/home_widget.dart';
import 'package:attendance_app/views/widgets/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class InitHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //print('>>> build homepage');

    return BlocBuilder<ClassroomsBloc, ClassroomsState>(
      builder: (context, state) {
        if (state is LoadingClassrooms) {
          return AppLoadingWidget(
            mess: 'Đang tải lớp học...',
          );
        } else if (state is FailedClassrooms) {
          return AppErrorPage(
            iconData: Icons.cloud_off,
            mess: 'Đã xảy ra lỗi',
            function: () =>
                BlocProvider.of<MQTTBloc>(context).add(ConnectMQTTService()),
          );
        } else if (state is LoadedClassrooms) {
          return BlocProvider(
            create: (context) =>
                MQTTBloc(BlocProvider.of<UpdateDataBloc>(context))
                  ..add(ConnectMQTTService(classrooms: state.classrooms)),
            child: ConnectMQTTWidget(),
          );
        }

        return null;
      },
    );
  }
}

class ConnectMQTTWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //print('>>> build conn');

    return BlocBuilder<MQTTBloc, MQTTState>(
      builder: (context, state) {
        if (state is LoadingState) {
          return AppLoadingWidget(
            mess: 'Kết nối server...',
          );
        } else if (state is ErrorState) {
          return AppErrorPage(
            iconData: Icons.cloud_off,
            mess: 'Lỗi kết nối',
            function: () =>
                BlocProvider.of<MQTTBloc>(context).add(ConnectMQTTService()),
          );
        } else if (state is ConnectedMQTT) {
          return BlocProvider(
            create: (context) => DrawerBloc()..add(DrawerMenuClicked(0)),
            child: HomeWidget(),
          );
        } else if (state is DisconnectedMQTT) {
          return AppErrorPage(
            iconData: Icons.close,
            mess: 'Đã ngắt kết nối',
            function: () =>
                BlocProvider.of<MQTTBloc>(context).add(ConnectMQTTService()),
          );
        }

        return null;
      },
    );
  }
}
