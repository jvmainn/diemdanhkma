
import 'package:attendance_app/model/models.dart';
import 'package:attendance_app/views/home/drawer_bloc/bloc.dart';
import 'package:attendance_app/views/home/fragments/about_us/about_us_fragment.dart';
import 'package:attendance_app/views/home/fragments/classrooms/classrooms_fragment.dart';
import 'package:attendance_app/views/widgets/widgets/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'children/drawer.dart';

class HomeWidget extends StatelessWidget {
  HomeWidget({this.classrooms});

  final List<Classroom> classrooms;

  @override
  Widget build(BuildContext context) {
   // print('>>> build home widget');
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
          statusBarColor: Colors.white,
          statusBarBrightness: Brightness.dark,
          statusBarIconBrightness: Brightness.dark,
          systemNavigationBarIconBrightness: Brightness.dark,
        ),
        child: Scaffold(
          drawer: DrawerMenu(),
          body: BlocBuilder<DrawerBloc, DrawerState>(
            builder: (context, state) {
              if (state is IndexDrawerClicked) {
                return IndexedStack(
                  index: state.index,
                  children: <Widget>[
                    ClassroomsFragment(),
                    AboutUsFragment()
                  ],
                );
              }
              return AppLoadingWidget();
            },
          ),
        ));
  }
}
