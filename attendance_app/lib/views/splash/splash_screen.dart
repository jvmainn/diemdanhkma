
import 'package:attendance_app/bloc/app_bloc/app_bloc.dart';
import 'package:attendance_app/bloc/app_bloc/bloc.dart';
import 'package:attendance_app/views/router/route_name.dart';
import 'package:attendance_app/views/widgets/widgets/loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AppLoadingWidget(mess: 'Kiểm tra đăng nhập...',);
  }
}
