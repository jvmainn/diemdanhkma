import 'package:attendance_app/app.dart';
import 'package:attendance_app/bloc/app_bloc/bloc.dart';
import 'package:attendance_app/bloc/mqtt_bloc/bloc.dart';
import 'package:attendance_app/configs/values/colors.dart';
import 'package:attendance_app/model/models.dart';
import 'package:attendance_app/views/profile/profile.dart';
import 'package:attendance_app/views/router/route_name.dart';
import 'package:attendance_app/views/classroom/classroom_page.dart';
import 'package:attendance_app/views/home/classrooms_bloc/bloc.dart';
import 'package:attendance_app/views/home/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

RouteFactory router() {
  return (RouteSettings settings) {
    Widget screen;

    var args = settings.arguments as Map<String, dynamic>;

    switch (settings.name) {
      case RouteName.home:
        return MaterialPageRoute(
          builder: (context) => BlocProvider.value(
            value: BlocProvider.of<ClassroomsBloc>(context)..add(FetchClassrooms()),
            child: InitHome(),
          ),
        );
      case RouteName.classroom:
        return MaterialPageRoute(
          builder: (context) =>  ClassroomPage(index: args['index'],),
        );
      case RouteName.profile:
        return MaterialPageRoute(builder: (_context)=> ProfilePerson(person: args['person'],));
      default:
        screen = FailedRouteWidget(settings.name);
        return MaterialPageRoute(
          builder: (_) => screen,
        );
    }
  };
}

class FailedRouteWidget extends StatelessWidget {
  FailedRouteWidget(this._name);

  final String _name;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Lạc đường rồi'),
      ),
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Icon(
                Icons.sentiment_neutral,
                size: 32,
                color: secondary,
              ),
              Text('Có vẻ bạn đã bị lạc đường $_name'),
            ],
          ),
        ),
      ),
    );
  }
}
